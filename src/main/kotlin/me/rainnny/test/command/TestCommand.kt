package me.rainnny.test.command

import org.bukkit.Bukkit
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player

class TestCommand : CommandExecutor {
    override fun onCommand(sender: CommandSender?, command: Command?, label: String?, args: Array<out String>?): Boolean {
        if (sender == null || command == null || label == null || args == null)
            return false
        if (args.isEmpty())
            sender.sendMessage("§cUsage: /test <player>")
        else {
            var target: Player? = Bukkit.getPlayer(args[0])
            if (target == null)
                sender.sendMessage("§cCould not find \"§f" + args[0] + "§c\"")
            else {
                target.sendMessage("hi, this is a test! Executed by: " + sender.name)
                sender.sendMessage("sent to: " + target.name)
            }
        }
        return true
    }
}