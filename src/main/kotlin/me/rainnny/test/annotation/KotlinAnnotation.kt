package me.rainnny.test.annotation

@Retention(AnnotationRetention.RUNTIME)
annotation class KotlinAnnotation (
    val name: String,

    val something: String = "hello"
)