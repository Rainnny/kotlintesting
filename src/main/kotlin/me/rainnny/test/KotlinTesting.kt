package me.rainnny.test

import me.rainnny.test.command.TestCommand
import me.rainnny.test.listeners.TestListener
import org.bukkit.Bukkit.getPluginManager
import org.bukkit.plugin.java.JavaPlugin

class KotlinTesting : JavaPlugin() {
    override fun onEnable() {
        getCommand("test").executor = TestCommand()
        getPluginManager().registerEvents(TestListener(), this)

        // Cool thing with Kotlin, is that you can code with Java and Kotlin at the same time
        // an example can be found below
        JavaClass.doSomething()
    }
}