package me.rainnny.test.listeners

import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerJoinEvent

class TestListener : Listener {
    @EventHandler
    fun onJoin(event: PlayerJoinEvent) {
        val player: Player = event.player
        player.sendMessage("Welcome to my server!")
    }
}