package me.rainnny.test.object;

public class JavaObject {
    private String fieldName;

    public JavaObject(String fieldName) {
        this.fieldName = fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getFieldName() {
        return fieldName;
    }
}