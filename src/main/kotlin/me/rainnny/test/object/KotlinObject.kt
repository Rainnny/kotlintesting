package me.rainnny.test.`object`

class KotlinObject(private var fieldName: String) {
    fun setField(fieldName: String) {
        this.fieldName = fieldName;
    }

    fun getField() : String {
        return fieldName;
    }
}