package me.rainnny.test.lambda

class KotlinLambda {
    companion object Static {
        fun runLambda() {
            val list = listOf("something", "something else", "last thing")
            list.filter { string -> string.contains("something") }.forEach { string -> println(string) }
        }
    }
}