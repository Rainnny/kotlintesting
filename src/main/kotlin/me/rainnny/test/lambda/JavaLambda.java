package me.rainnny.test.lambda;

import java.util.Arrays;
import java.util.List;

public class JavaLambda {
    public static void runLambda() {
        List<String> list = Arrays.asList("something", "something else", "last thing");
        list.stream().filter(string -> string.contains("something")).forEach(System.out::println);
    }
}